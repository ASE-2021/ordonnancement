#if !defined(SWAP)
#define SWAP

int store_to_swap(int, int);
int fetch_from_swap(int, int);

#endif // SWAP
