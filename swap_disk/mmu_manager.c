#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "hardware.h"
#include "hw_config.h"
#include "mmu_manager.h"
#include "swap.h"

#define PAGE_SWAPPED -2
#define EMPTY_VM_PAGE -1
#define EMPTY_PM_PAGE 0

int VM_MAPPING[VM_PAGES] = {EMPTY_PM_PAGE};
int PM_MAPPING[PM_PAGES] = {EMPTY_VM_PAGE};
int first_pm_free = 0;

extern void user_process();

void simple_swap_mmu_handler()
{
  tlb_entry_t tlb;
  int ppage = 1;
  long int vaddr = ((long int)_in(MMU_FAULT_ADDR_HI) << 32) | (_in(MMU_FAULT_ADDR_LO) & 0xFFFFFFFF);
  long int vpage = (vaddr >> 12) & 0xFFF;

  if (vaddr < ((long int)virtual_memory) || vaddr > ((long int)virtual_memory) + VM_SIZE)
  {
    fprintf(stderr, "Adresse virtuelle incorrecte");
    exit(EXIT_FAILURE);
  }

  /* Sauvegarder la vpage dans le fichier swap */
  store_to_swap(vpage, ppage);

  /* Rajouter l'entree dans la tlb */
  tlb.s.tlb_vpage = vpage;
  tlb.s.tlb_ppage = ppage;
  tlb.s.tlb_rwx = 7;
  tlb.s.tlb_used = 1;

  /* Vider la MMU */
  _out(MMU_CMD, MMU_RESET);
  _out(TLB_ADD_ENTRY, tlb.i);
}

void swap_mmu_handler()
{
  tlb_entry_t tlb;
  long int vaddr = (((long int)_in(MMU_FAULT_ADDR_HI)) << 32) | (((long int)_in(MMU_FAULT_ADDR_LO)) & 0xFFFFFFFF);
  int vpage = (vaddr >> 12) & 0xFFF;

  if (vaddr < (long int)virtual_memory || vaddr > (long int)virtual_memory + VM_SIZE - 1)
  {
    fprintf(stderr, "Adresse virtuelle incorrecte \n");
    exit(EXIT_FAILURE);
  }

  int ppage = VM_MAPPING[vpage];

  /*
   * Si la vpage n'a jamais eu de correspondance ou elle existe dans notre fichier de swap
   * sinon, on recupere directement sa ppage coorespondante dans nos mapping.
   */
  if (ppage == EMPTY_PM_PAGE || ppage == PAGE_SWAPPED)
  {
    /* round robin algorithm*/
    if (!(++first_pm_free % PM_PAGES))
      first_pm_free = 1;
    ppage = first_pm_free;

    /* old_vpage: la vpage qui pointait vers la ppage courante */
    /* S'il n'y a aucune vpage qui pointait vers la ppage courante
     * nous avons donc affaire à la première correspondance.
     */
    int old_vpage = PM_MAPPING[ppage];
    if (old_vpage != EMPTY_VM_PAGE)
    {
      store_to_swap(old_vpage, ppage);
      VM_MAPPING[old_vpage] = PAGE_SWAPPED; /* On signifie qu'on l'a rajouté dans le swap*/
    }

    /* Si la vpage courant existe dans notre fichier swap,
     * On le restore dans ce cas.
     * */
    if (VM_MAPPING[vpage] == PAGE_SWAPPED)
      fetch_from_swap(vpage, ppage);

    VM_MAPPING[vpage] = ppage;
    PM_MAPPING[ppage] = vpage;
  }

  tlb.s.tlb_vpage = vpage;
  tlb.s.tlb_ppage = ppage;
  tlb.s.tlb_rwx = 7;
  tlb.s.tlb_used = 1;

  _out(TLB_DEL_ENTRY, tlb.i);
  _out(TLB_ADD_ENTRY, tlb.i);
}

void master_process()
{
  if (init_hardware(HARDWARE_INI) == 0)
    fprintf(stderr, "Error in hardware initialization\n");
  IRQVECTOR[MMU_IRQ] = swap_mmu_handler;
}

int main(int argc, char **argv)
{
  master_process();
  _mask(0x1001);
  user_process();

  return 0;
}
