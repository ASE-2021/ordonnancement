#if !defined(MI_KERNEL)

#define MI_KERNEL

#define MMU_ENABLE 1
#define MMU_IRQ 13
#define MMU_FAULT_ADDR_LO 0xCC
#define MMU_FAULT_ADDR_HI 0xCD
#define TLB_ADD_ENTRY 0xCE
#define MMU_CMD 0x66
#define HARDWARE_INI "hardware.ini"

struct tlb_entry_s
{
    unsigned tlb_RFU : 8;
    unsigned tlb_vpage : 12;
    unsigned tlb_ppage : 8;
    unsigned tlb_rwx : 3;
    unsigned tlb_used : 1;
};

union tlb_entry_u
{
    int i;
    struct tlb_entry_s s;
};

typedef union tlb_entry_u tlb_entry_t;

void simple_swap_mmu_handler();
void swap_mmu_handler();
void init_master();

#endif
