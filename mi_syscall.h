#if !defined(MI_SYSCALL)
#define MI_SYSCALL

#define SYSCALL_SWTCH_0 16
#define SYSCALL_SWTCH_1 17

#define N 127

void init(void);

#endif // MI_SYSCALL
