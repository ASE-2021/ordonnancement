#if !defined(TRY)
#define TRY

#define TIMER_PARAM 0xF4
#define TIMER_ALARM 0xF8
#define TIMER_IRQ 2
#define HARDWARE_INI "hardware.ini"

typedef int(func_t)(int);
typedef void(funct_t)(void *);

typedef enum
{
    CTX_INIT,
    CTX_EXEC,
    CTX_END
} ctx_status_t;

typedef struct ctx_s
{
    void *rsp;
    void *rbp;
    char *stack;
    funct_t *f;
    void *args;
    ctx_status_t status;
    struct ctx_s *next_ctx;
} ctx_t;

int try(ctx_t *, func_t *, int);

int throw(ctx_t *, int);

int init_ctx(ctx_t *, int, funct_t, void *);

void switch_to_ctx(ctx_t *);

void start_ctx();

ctx_t *create_ctx(int, funct_t, void *);

void yield();

void start_sched();

void irq_disable();

void irq_enable();

#endif // TRY
