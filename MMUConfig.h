#define MMU_ENABLE 1
#define MMU_IRQ	13
#define MMU_CMD		 0x66
#define MMU_FAULT_ADDR	 0xCD
#define TLB_ADD_ENTRY	 0xCE
#define TLB_DEL_ENTRY	 0xDE
#define TLB_SIZE       	 32		
#define TLB_ENTRIES	 0x800		       
