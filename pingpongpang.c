#include <stdio.h>
#include <stdlib.h>
#include "try.h"

#define STACK 16384

void f_ping(void *args);
void f_pong(void *args);
void f_pang(void *args);

int main(int argc, char *argv[])
{
    ctx_t *pang_ctx = create_ctx(STACK, f_pang, NULL);
    ctx_t *pong_ctx = create_ctx(STACK, f_pong, NULL);
    ctx_t *ping_ctx = create_ctx(STACK, f_ping, NULL);
    printf("Begin \n");
    //yield();
    start_sched();
    printf("Nope \n");
    return EXIT_SUCCESS;
}

void f_ping(void *args)
{
    //irq_disable();
    while (1)
    {
        printf("A");
        printf("B");
        printf("C");
    }
}

void f_pong(void *args)
{
    while (1)
    {
        printf("1");
        printf("2");
    }
}

void f_pang(void *args)
{
    while (1)
    {
        printf("(");
        printf(":");
        printf(")");
    }
}
