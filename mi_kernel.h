#if !defined(MI_KERNEL)
#define MI_KERNEL

#define VM_SIZE 4000 * 4096
#define PM_SIZE 4000 * 256
#define MMU_FAULT_ADDR_LO 0xCC
#define MMU_FAULT_ADDR_HI 0xCD

#define PROCESS_1 1
#define PROCESS_0 0

struct tlb_entry_s
{
    unsigned tlb_RFU : 8;
    unsigned tlb_vpage : 12;
    unsigned tlb_ppage : 8;
    unsigned tlb_rwx : 3;
    unsigned tlb_used : 1;
};

union tlb_entry_u
{
    int i;
    struct tlb_entry_s s;
};

typedef union tlb_entry_u tlb_entry_t;

void mmu_handler();

int simple_swap_mmu_handler(void);

int swap_mmu_handler(void);

void switch_to_process0(void);

void switch_to_process1(void);

void main_master(void);

#endif // MI_KERNEL
