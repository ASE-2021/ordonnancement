#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hardware.h"
#include "mi_kernel.h"
#include "mi_user.h"
#include "MMUConfig.h"
#include "mi_syscall.h"

int current_process = 0;

void mmu_handler()
{
    tlb_entry_t tlb;
    long int vaddr = ((long int)_in(MMU_FAULT_ADDR_HI) << 32) | (_in(MMU_FAULT_ADDR_LO) & 0xFFFFFFFF);
    long int vpage = (vaddr >> 12) & 0xFFF;
    long int ppage = ppage_of_vpage(current_process, vpage);

    //printf("%lx, %lx, %lx, %lx, %d\n", vpage, ppage, vaddr, (long int)virtual_memory, current_process);
    if (vaddr < ((long int)virtual_memory) || vaddr > ((long int)virtual_memory) + VM_SIZE || ppage == -1)
    {
        fprintf(stderr, "Echec"); // TODO
        exit(EXIT_FAILURE);
    }

    tlb.s.tlb_vpage = vpage;
    tlb.s.tlb_ppage = ppage;
    tlb.s.tlb_rwx = 7;
    tlb.s.tlb_used = 1;

    _out(TLB_ADD_ENTRY, tlb.i);
    //_out(TLB_ADD_ENTRY, *(int *)(&tlb));
}

void switch_to_process0(void)
{
    current_process = 0;
    _out(MMU_CMD, MMU_RESET);
}

void switch_to_process1(void)
{
    current_process = 1;
    _out(MMU_CMD, MMU_RESET);
}

void init()
{
    if (init_hardware("MMUConfig.ini") == 0)
    {
        fprintf(stderr, "ERROR INITHARDWARE");
        exit(EXIT_FAILURE);
    }
    IRQVECTOR[16] = switch_to_process0;
    IRQVECTOR[17] = switch_to_process1;
    IRQVECTOR[MMU_IRQ] = mmu_handler;
}

int ppage_of_vpage(int process, unsigned vpage)
{
    if (vpage < N)
    {
        if (process == PROCESS_0)
            return 2 * vpage + 2;
        if (process == PROCESS_1)
            return 2 * vpage + 1;
    }

    return -1;
}

void main_master(void)
{
    init();
    IRQVECTOR[16] = switch_to_process0;
    IRQVECTOR[17] = switch_to_process1;
}

int main(int argc, char **argv)
{

    main_master();
    _mask(0x1001); // Basculer mode utilisateur
    main_user();
}