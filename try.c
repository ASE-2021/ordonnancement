#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "try.h"
#include "hardware.h"

ctx_t *current_ctx = NULL;
ctx_t *ring_ctx = NULL;

int try(ctx_t *pctx, func_t *f, int arg)
{
    asm("mov %%rsp, %0"
        "\n\t"
        "mov %%rbp, %1"
        : "=r"(pctx->rsp), "=r"(pctx->rbp));

    return f(arg);
}

int throw(ctx_t * pctx, int r)
{
    assert(pctx != NULL);

    static int r_tmp;
    r_tmp = r;

    asm("mov %0 ,%%rsp"
        "\n\t"
        "mov %1 ,%%rbp "
        :
        : "r"(pctx->rsp), "r"(pctx->rbp));

    return r_tmp;
}

int init_ctx(ctx_t *pctx, int stack_size, funct_t f, void *args)
{
    pctx->stack = malloc(stack_size);
    pctx->rsp = pctx->stack + stack_size - sizeof(void *);
    pctx->rbp = pctx->stack + stack_size - sizeof(void *);
    pctx->f = f;
    pctx->args = args;
    pctx->status = CTX_INIT;
}

void switch_to_ctx(ctx_t *pctx)
{
    if (pctx->status == CTX_END)
    {
        // Si c'est l'unique ctx
        if (pctx == pctx->next_ctx)
            ring_ctx = NULL;
        else
            current_ctx->next_ctx = pctx->next_ctx;

        free(pctx->stack);
        free(pctx);
        yield();
        return;
    }

    assert(pctx->status != CTX_END);
    if (current_ctx != NULL)
    {
        // Sauvegarder où on en est avec le ctx courant
        asm("mov %%rsp, %0"
            "\n\t"
            "mov %%rbp, %1"
            : "=r"(current_ctx->rsp), "=r"(current_ctx->rbp));
    }

    current_ctx = pctx;
    asm("mov %0, %%rsp"
        "\n\t"
        "mov %1, %%rbp"
        :
        : "r"(current_ctx->rsp), "r"(current_ctx->rbp));

    if (current_ctx->status == CTX_INIT)
        start_ctx();
}

void start_ctx()
{
    current_ctx->status = CTX_EXEC;
    irq_enable();
    current_ctx->f(current_ctx->args);
    current_ctx->status = CTX_END;
    yield();
}

ctx_t *create_ctx(int stack_size, funct_t f, void *args)
{
    ctx_t *new_ctx = malloc(sizeof(ctx_t));

    if (ring_ctx == NULL)
    {
        new_ctx->next_ctx = new_ctx;
        ring_ctx = new_ctx;
    }
    else
    {
        new_ctx->next_ctx = ring_ctx->next_ctx;
        ring_ctx->next_ctx = new_ctx;
    }

    init_ctx(new_ctx, stack_size, f, args);
    return new_ctx;
}

void yield()
{
    irq_disable();
    if (ring_ctx == NULL)
    {
        printf("\nEnd \n");
        exit(EXIT_SUCCESS);
    }

    if (current_ctx == NULL)
        switch_to_ctx(ring_ctx->next_ctx);
    else
        switch_to_ctx(current_ctx->next_ctx);

    irq_enable();
}

void empty_it()
{
    return;
}

void timer_it()
{
    _out(TIMER_ALARM, 0xFFFFFFFE); /* -1 */
    yield();
}

void start_sched()
{
    unsigned int i;

    /* init hardware */
    if (init_hardware(HARDWARE_INI) == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* dummy interrupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* program timer */
    IRQVECTOR[TIMER_IRQ] = timer_it;
    _out(TIMER_PARAM, 128 + 64 + 32 + 8); /* reset + alarm on + 8 tick / alarm / 11101000 */
    _out(TIMER_ALARM, 0xFFFFFFFE);        /* alarm at next tick (at 0xFFFFFFFF) */

    /* allows all IT */
    _mask(1);

    yield();
}

void irq_disable()
{
    _mask(15); // blocks all IT
}

void irq_enable()
{
    _mask(1); // allows all IT
}