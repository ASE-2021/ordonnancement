INC=-I/vagrant/ASE/ordonnancement/x86-64/include 
LIB=-L/vagrant/ASE/ordonnancement/x86-64/lib -lhardware
 
all: try_mul pingpong pingpongpang mi_kernel

display_stack : display_stack.o
	gcc -o $@ $^ $(LIB)

try_mul : try.o try_mul.o
	gcc -o $@ $^ $(LIB)

pingpong : try.o pingpong.o
	gcc -o $@ $^ $(LIB)

pingpongpang : try.o pingpongpang.o
	gcc -o $@ $^ $(LIB)

mi_kernel : mi_user.o mi_kernel.o
	gcc -o $@ $^ $(LIB)

%.o:%.c
	gcc $(INC) -c $<

clean:
	rm -f *.o *.s display_stack try_mul pingpong pingpongpang vdisk?.bin mi_kernel