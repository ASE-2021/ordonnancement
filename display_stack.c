#include <stdio.h>

int display(int pa, int pb, int pc)
{
    printf("pa = %p; pb = %p; pc = %p\n", &pa, &pb, &pc);
    return 0;
}

int main()
{
    int a, b;
    int la, lb, lc;

    asm("mov %%rsp, %0"
        "\n\t"
        "mov %%rbp, %1"
        : "=r"(a), "=r"(b));

    printf("rsp = %p; rbp = %p\n", &a, &b);
    printf("la = %p; lb = %p; lc = %p\n", &la, &lb, &lc);

    return display(la, lb, lc);
}
