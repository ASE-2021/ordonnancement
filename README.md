# TPs ASE

**Auteurs:** Thierno-Souleymane BAH et Raoult BALLOT

## Ordonnancement

### TP1

Les instructions suivantes vous permettront de tester notre travail:

- Compiler le projet

```bash
    $ make
```

- Exécuter display_stack

```bash
    $ ./display_stack
```

- Exécuter try_mul

```bash
    $ ./try_mul
```

- Exécuter ping pong

```bash
    $ ./pingpong
```

### TP2

Les fonctions create_ctx et yield ont été implementé, la strcuture ctx et la fonction switch_to_ctx ont été adapté en conséquence. </br>
Nous avons créé un programme pingpongpang qui permet de switcher vers 3 contextes automatiquement (ordonnancement explicite). </br>
Veuillez saisir les commandes suivantes pour tester notre travail.

- Compiler le projet

```bash
    $ make pingpongpang
```

- Exécuter pingpongpang

```bash
    $ ./pingpongpang
```

### TP3

Les fonctions start_sched, irq_disable et irq_enable ont été implementé. </br>
Nous avons adapté, en conséquence, le programme pingpongpang permettant de switcher automatiquement vers 3 contextes. </br>
Les commandes à exécuter restent les mêmes que celles du TP2.

### TP4

La structure tlb_entry representant la TLB a été implementé ainsi que les fonctions ppage_of_vpage et mmu_handler. </br>
Nous n'avons pas trouvé un scenario de test pour les fonctions implementés en particulier pour la fonction mmu_handler. </br>
Nous esperons, dans les TPs prochains, les tester et s'assurer qu'elles fonctionnent parfaitement.

### TP5

Les fonctions necessaires au bon fonctionnement du swap disk ont été implementé.
Nous avons pu comme expliqué dans l'énoncé et tout fonctionne sans problème.
Veuillez saisir les commandes suivantes pour tester notre projet.

```bash
    $ cd swap_disk && make
```

```bash
    $ ./mmu_manager | tee /dev/stderr | ./oracle
```

Vous devez voir afficher OK à la fin de l'exécution, sinon KO sera affiché.
