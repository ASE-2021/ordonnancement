#if !defined(MI_USER)
#define MI_USER

#define PAGE_SIZE 4000

int ppage_of_vpage(int process, unsigned vpage);

int sum(void *ptr);

void main_user(void);

#endif // MI_USER
